package com.flipsale.com.recyleview_drag_drop;

import com.flipsale.com.Uploader.ProductImageDatas;
import com.flipsale.com.utility.CapturedImage;

import java.util.List;

/**
 * Created by embed on 25/6/18.
 */

public interface OnCustomerListChangedListener {
    // use while post product
    void onNoteListChanged(List<CapturedImage> capturedImageList);
    // use while update product
    void onUpdateImagesListChanged(List<ProductImageDatas> productImageDatasList);
}
